# 1. Grundlagen

- #### Was ist Q
<details>
<summary><strong>Antwort</strong></summary>

      Die Elektrische Ladung
</details>

- #### Technische Stromrichtung?
<details>
<summary><strong>Antwort</strong></summary>

      Von Plus nach Minus
</details>

- #### Was besagt die 1. Kirchhoffsche Maschenregel?
<details>
<summary><strong>Antwort</strong></summary>

      1. Ladungserhaltung
      2. Aus einem Knoten fließt genauso viel heraus, wie in ihn hineinfließt
<img src="img/questions/q4.png" height="auto" width="auto">
<img src="img/questions/q4a.png" height="auto" width="auto">
</details>

- #### Was besagt die 2. Kirchhoffsche Maschenregel?
<details>
<summary><strong>Antwort</strong></summary>

      1. Masche ist die Summe aller aller Spannungen an Bauteilen, die einen geschlossenen Kreis(Masche) bilden.
      2. Die Summe aller Spannungen ist immer Null
<img src="img/questions/q5.png" height="auto" width="auto">
<img src="img/questions/q5a.png" height="auto" width="auto">
</details>

# 2. Passive Bauteile

- #### Was ist ein Unbelasteter Spannungsteiler
<details>
<summary><strong>Antwort</strong></summary>

      Riehenschaltung
</details>

- #### Was ist ein Belasteter Spannungsteiler
<details>
<summary><strong>Antwort</strong></summary>

      Paralellschaltung
</details>

- #### Was machen Spulen?
<details>
<summary><strong>Antwort</strong></summary>

      Erzeugen Magnetfeld --> INDUKTIVITÄT
</details>

- #### Was macht ein Relai?
<details>
<summary><strong>Antwort</strong></summary>

      1. Schaltet hohe Lasten mit kleinen Steuerströmen
      2. Switch über mechanischen Schalter
      3. IMMER mit Freilaufdiode betreiben
</details>

- #### Welche ist ein schließender und was ist ein öffnender Taster
<details>
<summary><strong>Antwort</strong></summary>

      1. Schließend: beim drücken fließt strom
      2. Öffnend: beim drücken stoppt strom
</details>

- #### Was muss man nehmen, um einen Taster umzulegen? zb auf von 0 auf 1?
<details>
<summary><strong>Antwort</strong></summary>

      1. R-S FlipFlop
</details>

# 3. Aktive Bauteile

# 4. MOSFETS

# 5. Wechselstrom
- #### Widerstand an Wechselspannung --> Auswirkung auf Phasenverschiebung?
<details>
<summary><strong>Antwort</strong></summary>

      Wiederstand hat KEINE Auswirkung auf die Phasenverschiebung
<img src="img/5.Wechselstrom/w4.png" height="auto" width="auto">
</details>

- #### Kondensator an Wechselspannung --> Auswirkung auf Phasenverschiebung?
<details>
<summary><strong>Antwort</strong></summary>

      Kondensator HAT Auswirkung auf die Phasenverschiebung
  <img src="img/5.Wechselstrom/w6.png" height="auto" width="auto">
</details>

- #### Für Kondensatoren gilt für den komplexen Widerstand:
<details>
<summary><strong>Antwort</strong></summary>

      Bei steigender Frequenz oder bei steigender Kapazität SINKT der komplexe Widerstand
</details>

- #### Für Spulen gilt für den komplexen Widerstand:
<details>
<summary><strong>Antwort</strong></summary>

      Bei steigender Frequenz oder bei steigender Induktivität STEIGT der komplexe Widerstand
</details>

- #### Wann verringert ein Kondensator seinen komplexen Widerstand?
<details>
<summary><strong>Antwort</strong></summary>

      Bei Steigender Frequenz
</details>

- #### Wann vergrößert eine Spule ihren komplexen Widerstand?
<details>
<summary><strong>Antwort</strong></summary>

      Bei Steigender Frequenz
</details>

- #### Wie lautet die Formel für Leistung? P=
<details>
<summary><strong>Antwort</strong></summary>

      P=U*I
</details>

# 6. Operationsverstärker

- #### Welche Eigenschaften hat ein Operationsverstärker?
<details>
<summary><strong>Antwort</strong></summary>

      1. Keine Eingangsströme
      2. Ausgangsstrom beliebig groß
      3. Verstärkung der Differenzspannung Ud unendlich --> Ud = 0V
</details>

- #### Wie lautet die Formel für den Verstärkungsfaktor K bei Nichtinvertierenden Verstärkern?
<details>
<summary><strong>Antwort</strong></summary>

 <img src="img/questions/q1.png" height="auto" width="auto">
</details>

- #### Wie lautet die Formel für den Verstärkungsfaktor K bei Invertierenden Verstärkern?
<details>
<summary><strong>Antwort</strong></summary>

 <img src="img/questions/q2.png" height="auto" width="auto">
</details>

- #### Was macht ein Komparator?
<details>
<summary><strong>Antwort</strong></summary>

      1. Bekommt HIGH-Pegel beim Überschreiten der Eingangsspannungn (Temperatur etc)
      2. Gibt dann HIGH am Ausgang aus
 <img src="img/questions/q3.png" height="auto" width="auto">
</details>

- #### Was macht ein Impedanzwandler?
<details>
<summary><strong>Antwort</strong></summary>

    1. Entkoppelt eine Schaltung
    2. Eingangsstrom ist Null
    3. Hochohmiger Eingang, Niedrigohmiger Ausgang ("Wandlung der Impdanz")
<img src="img/6.Operationsverstärker/o8.png" height="auto" width="auto">
</details>

# 7. Digitaltechnik
