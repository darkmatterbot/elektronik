# Elektronik

<details>
<summary><strong>1. Grundlagen</strong></summary>
<br/>

### Ladung
- **Q = Elektrische Ladung**
- Q wird in Coulomb (C) oder Amperesekunden (As) gemessen
- Jede elektrische Ladung ist ein Vielfaches der Elementarladung e = 1,6*10^(-9)C
- Ladungsmengen sind immer Vielfaches der Elementarladung
- Ladungen sind positiv oder negativ
- **Gleichnamige Ladungen stoßen sich ab, Entgegesetzte Ladungen ziehen sich an --> Stromfluß**
- Metalle lassen Elektronen leicht verschieben --> Fluß von negativen Ladungen
- Nichtleiter lassen wenig bis kein Stromfluss durch

### Stromstärke
- **I = Stromstärke**
- **I = dQ/dt**
- Die **Stromstärke (I)** ist die Ladungsmenge **dQ**, die im Zeitintervall **dt** fließt.
- **Zeitabhändige** Ströme: <img src="img/integral.png" height="auto" width="auto">
- **Zeitlich konstanter** Strom: I = Q/t
<img src="img/1.Grundlagen/ladungstrager.png" height="auto" width="auto">

### Spannung
- Spannung ist die ursache des Ladungstransports, also des Stromflusses
- Getrennte Ladungen erzeugen ein elektrisches Feld mit der Feldstärke E
- Spannung ist definiert als Linienintegral über die elektrische Feldstärke E entlang des Wegelements ds <img src="img/1.Grundlagen/integral2_spannung.png" height="auto" width="auto">
- Die elektrische Feldstärke ist definiert duch die Kraft F, die auf eine Probeladung Q einwirkt <img src="img/1.Grundlagen/formel_spannung.png" height="auto" width="auto">
- Elektrische Feldstärke zwischen 2 Polen
- Spannung ist die **Potentialdifferenz** zwischen zwei Punkten p1 und p2 mit den Potenzialen phi1 und phi2. Die Spannung ist folglich: <img src="img/1.Grundlagen/formel2_spannung.png" height="auto" width="auto">
- <img src="img/1.Grundlagen/feldstark.png" height="auto" width="auto">
- Vergleich Wassersäule:
</br>
<img src="img/1.Grundlagen/wassersaule.png" height="auto" width="auto">

### Technische Stromrichtung
- Die **Technische Stromrichtung** geht **von Plus nach Minus**
- Die **Elektronen fließen von Minus (Elektronenspender) nach Plus**
- <img src="img/1.Grundlagen/stromrichtung.png" height="auto" width="auto">

### Wiederstand
- Wiederstand **R** ist ein Maß für die Hemmung des Ladungstransports
- **Der elektrische Wiederstand R beträgt 1 Ohm wenn zwischen zwei Punkten eines Leiters bei einer Spannung von 1V genau 1A fließt.**
- Der Kehrwert beschreibt den Leitwert in Siemens <img src="img/1.Grundlagen/siemens_leitwert.png" height="auto" width="auto">
- Der elektrische Wiederstand eines Leiters der länge l mit dem Querschnitt A is <img src="img/1.Grundlagen/formel_wiederstand.png" height="auto" width="auto">
- Der Wiederstand eines metallischen Leiters ist **Temperaturabhängig**. Näherungsweise gilt: <img src="img/1.Grundlagen/formel_wiederstand2.png" height="auto" width="auto">
- Der Temperaturkoeffizient **at0** gibt an, welche relative Wiederstandsänderung der Leiter bei einer Temperatur **t** hat.
- Materialkonstanten <img src="img/1.Grundlagen/materialkonstanten.png" height="auto" width="auto">

### Magnetismus
- <img src="img/1.Grundlagen/spule.png" height="auto" width="auto">
- **Magnetismus** <img src="img/1.Grundlagen/magnetismus.png" height="auto" width="auto">
- **Magnetfeld** des stromdurchflossenen Leiters <img src="img/1.Grundlagen/magnetfeld.png" height="auto" width="auto">
- Magnetische **Feldstärke** <img src="img/1.Grundlagen/feldstarke.png" height="auto" width="auto">
- Elektrische **Durchflutung** <img src="img/1.Grundlagen/durchflutung.png" height="auto" width="auto">
- Kraft auf Stromdurchflossenem Leiter <img src="img/1.Grundlagen/magnetfeld2.png" height="auto" width="auto">
- **Lorentzkraft** <img src="img/1.Grundlagen/lorentz.png" height="auto" width="auto">
- kraft auf ein sich bewegendes geladenes Teilchens im Magnetfeld <img src="img/1.Grundlagen/lkraft.png" height="auto" width="auto">
- Vektorschreibweise Lorentzkraft <img src="img/1.Grundlagen/lorentz2.png" height="auto" width="auto">

### Induktionsgesetz
- Induktionsgesetz der Bewegung <img src="img/1.Grundlagen/induktionsgesetz.png" height="auto" width="auto">
- Induktionsgesetz ohne Bewegung <img src="img/1.Grundlagen/induktionsgesetz3.png" height="auto" width="auto">
<img src="img/1.Grundlagen/induktionsgesetz2.png" height="auto" width="auto">

### Lenz'sche Regel
- Induzierte Spannung führ zu Strom durch den Wiederstand R
- Kraftwirkung auf Leiter im Magnetfeld wirk Bewegungsrichtung entgegen
- Bei größer werdendem Fluss führt induzierte Spannung dazu, dass zunehmender Fluss entgegen gewirkt wird <img src="img/1.Grundlagen/lorentzregel2.png" height="auto" width="auto">
<img src="img/1.Grundlagen/lorentzregel.png" height="auto" width="auto">

### Selbstinduktion
- Erhöht sich der Strom **I**, so ändert sich der Fluss. Eine Spannung wird induziert, die der Zunahme des Flusses <img src="img/1.Grundlagen/phi.png" height="auto" width="auto"> entgegenwirkt.
<img src="img/1.Grundlagen/selbstinduktion.png" height="auto" width="auto">
<img src="img/1.Grundlagen/selbstinduktion2.png" height="auto" width="auto">

### Magnetische Kopplung
- Befindet sich in der Nähe einer stromdurchführenden Spule eine zweite Spule, dann durchdringt Magnetischer Fluss beide Spulen. Beide sind **magnetisch gekoppelt --> gegenseitige Induktion**

### Ohmesches Gesetz
- In einem metallischen Leiter nimmt der Strom **I** proportional der angelegten Spannung **U** zu, so dass das Ohmesche Gesetz gilt: <img src="img/1.Grundlagen/ohm1.png" height="auto" width="auto">
- Die **U-I**-Kennlinie gibt Auskunft darüber, wie sich die Spannung bei Änderung des Stroms bzw. der Strom bei Änderung der Spannung in einem Bauteil/Leitung verhält.
<img src="img/1.Grundlagen/ohm2.png" height="auto" width="auto">
- Beispiele für **U-I**-Kennlinien
<img src="img/1.Grundlagen/ohm3.png" height="auto" width="auto">

### Bildzeichen
- <img src="img/1.Grundlagen/bildzeichen.png" height="auto" width="auto">
- Halbleiter: <img src="img/1.Grundlagen/bildzeichen2.png" height="auto" width="auto"><img src="img/1.Grundlagen/bildzeichen3.png" height="auto" width="auto">

### Kirchhoffschen Regeln
- Elektrische Netzwerke bestehen aus **Knoten** und **Maschen**.
- ### 1. kirchhoffsche Regel: **Maschenregel**
  - Knoten sind Punkte, wo mehrere Bauteile "zusammengebaut" sind, also eine Verbindung haben. Nach dem Gesetz der Ladungserhaltung, fließt aus einem Knoten immer genau so viel heraus, wie in ihn hineinfließt.
- ### 2. Kirchhoffsche Regel: **Knotenregel**
  - Als Maschen bezeichnet man die Summe aller Spannungen an Bauteilen, die einen geschlossenen Kreis (Masche) bilden. Die Summe aller Spannungen ist immer Null.
<img src="img/1.Grundlagen/kirchhoff.png" height="auto" width="auto">
</details>

<details>
<summary><strong>2. Passive Bauteile</strong></summary>
<br/>

### Wiederstände
- Toleranzringe <img src="img/2.Passive_Bauteile/resistors.png" height="auto" width="auto">
- 4 bar Wiederstände <img src="img/2.Passive_Bauteile/4bar.png" height="auto" width="auto">
- 5 bar Wiederstände <img src="img/2.Passive_Bauteile/5bar.png" height="auto" width="auto">
- Um Werte vverschiedener Reihen, bzw. mit verschiedenen Abstufungen darzustellen --> 4/5 Bar <img src="img/2.Passive_Bauteile/bars.png" height="auto" width="auto">

### SMD-Wiederstände (Surface Mounted Device)
- Vorteile:
  - Einfach bestückbar, kleinerer Bauraum
  - Zeit- und Kostenersparnis
  - keine Drahtabfälle
- Nachteile:
  - Spezialwerkzeuge fürs reparieren notwendig
  - von Hand löten schwierig aufgrund der kleinen Größe
  <br/>
  <img src="img/2.Passive_Bauteile/smd.png" height="auto" width="auto">

### Kirchhoff
- 1. Kirchhoffsches Gesetz - **Knotenregel**
  - Alle Ströme, die in einen Knoten hineinfließen, ergeben die Summe 0 (alle Ströme die in einen Punkt hineinfließen, fließen auch wieder raus, nicht mehr und nicht weniger).
  <img src="img/2.Passive_Bauteile/kh3.png" height="auto" width="auto">
  <img src="img/2.Passive_Bauteile/kh2.png" height="auto" width="auto">

- 2. Kirchhoffsches Gesetz - **Maschenregel**
  - Alle Spannungen in einem "Kreis" ergeben in Summe 0 (keine Spannung kann verloren gehen oder entstehen)
    <br/>
  <img src="img/2.Passive_Bauteile/uri.png" height="auto" width="auto">
  <img src="img/2.Passive_Bauteile/kh.png" height="auto" width="auto">

### Potentiometer
- **Potentiometer sind veränderbare Wiederstände**
- Mit Potentiometern werden **analoge Werte eingestellt** --> Analog wie Software-variablen.
- <img src="img/2.Passive_Bauteile/poti.png" height="auto" width="auto">
- **Wiederstandsverläufe:**
  - **Linear** zb 100kOhm Poti --> 180° = 50kOhm --> Wiederstand variiert zum Winkel
  - **Logarithmisch** z.b. Audiotechnik
  - Problem: Linearer Lautsärke verlauf fühlt sich sprunghaft an
  - Kann als **Spannungsteiler** eingesetzt werden <img src="img/2.Passive_Bauteile/poti_spannungsquelle.png" height="auto" width="auto">
  - Beispiel:
    - **Ub = 9V, R1 = 100kOhm, R2 = 3.6kOhm**
    <br/>
    U2 = R2*I = R2/(R1+R2)*Ub = 3.6kOhm/(10kOhm + 3.6kOhm)*9V = 3.6/(13.6)*9V = 2.38V

#### Poti als **Spannungsteiler**
- **Unbelastet** --> Wiederstände in Reihe
- **Belastet** --> Wiederstände Parallel

### Kondensatoren
- <img src="img/2.Passive_Bauteile/kondensator.png" height="auto" width="auto">
- Speichern Ladung
- Kapazität **C** wird in **Farad (F)** angegeben
- <img src="img/2.Passive_Bauteile/kondensator2.png" height="auto" width="auto">
- **Elektroly-Kondensatoren (Elko)**
    - **Gepolter** Kondensator
    - Anoden-Elektrode (+Pol) besteht aus einem Ventilmetall
    - Kathode ist Elektrolyt
    - Falsche Polung --> **Bauteilexplosion**
    <br/>
    <img src="img/2.Passive_Bauteile/kondensator3.png" height="auto" width="auto">
    - **Drehkondensatoren**
      - Kleine Kapazität
      - Heute meist durch Halbleiter ersetzt
    - **Supercaps**
      - Speichern **viel Ladung (mehrere Farad)**.
      - Können **sehr oft geladen/entladen** werden.
    - Schaltung
      - <img src="img/2.Passive_Bauteile/kondensator4.png" height="auto" width="auto">
      - <img src="img/2.Passive_Bauteile/kondensator5.png" height="auto" width="auto">
    - Formeln
      - <img src="img/2.Passive_Bauteile/kondensator6.png" height="auto" width="auto">

### Spulen
- <img src="img/2.Passive_Bauteile/spule.png" height="auto" width="auto">
- Stromdurchfluss erzeugt Magnetfeld --> **Induktivität**
- Durch einen Eisenkern (ferromagnetisch) in der Mitte der Spule wird das Magnetfeld verstärkt
- Einheit der Induktivität **L** ist **Henry (H)**

### Trafo
- <img src="img/2.Passive_Bauteile/trafo3.png" height="auto" width="auto">
- Transformatoren bestehen aus zwei Spulen (**Primär- und Sekundärspule**) auf einem geblechten Kern.
- Legt man Wechselspannung an, so wird ein **magnetisches Wechselfeld** erzeugt, das durch beide Spulen fließt.
- Durch das **Wicklungsverhältnis entsteht ein Spannungsverhältnis**.
- <img src="img/2.Passive_Bauteile/trafo.png" height="auto" width="auto">
- **Trafos** sind auch mit mehreren ABgriffen und somit **mehreren Sekundärspulen** erhältlich.
- Bsp: Soll eine symmetrische Spannungsversorgung aufgebaut werden, so kann ein Trafo mit 2 Sekundärspulen verwendet werden.
- <img src="img/2.Passive_Bauteile/trafo2.png" height="auto" width="auto">
- **Innenwiederstand**
  - Die Spulen bestehen aus metallischen Leitungen (Kupfer) und haben damit einen Wiederstand.
  - Der Wiederstand der Sekundärspule stellt somit den **Innenwiederstand der Spannungsquelle** dar, wenn eine Last an der Sekundärspule angehängt wird.
  - <img src="img/2.Passive_Bauteile/trafo4.png" height="auto" width="auto">
- **Klemmenspannung**
  - <img src="img/2.Passive_Bauteile/trafo5.png" height="auto" width="auto">

### Übertrager
- <img src="img/2.Passive_Bauteile/ubertrager.png" height="auto" width="auto">
- Übertrager dienen dazu, **eine Wechselspannung** (Signale aus der Audio/Mess-Technik) **von einem Stromkreis zu einem anderen Stromkreis zu übertragen** und dabei ggf. die Spannungspegel anzupassen.
- **Technischer Einsatz:**
  - **Leistungsstarke Übertrager**: Verstärker (Röhren, Spannungsinverter)
  - **Signalübertrager**: Galvanische Trennung, Unterdrückung von Brummschleifen und Kriechströmen

### Relais
- <img src="img/2.Passive_Bauteile/relai.png" height="auto" width="auto">
- Elektromagnet sieht Metall an, an dem ein mechanischer Schalter angebracht ist.
- Hauptaufgabe: Schalten von **hohen Lasten mit kleinen Steuerströmen**
- Schalt-Stromkreis ist getrennt von Lastenstromkreis (**galvanische Trennung**)
- **Schaltspannung muss passend** gewählt werden, sonst brennt die Spule durch oder die Spule erzeugt nicht genug Feldstärke, um den Schalter zu schließen.
- Der Schaltkontakt darf nur mit der angegebenen maximalen Spannung/Stromstärke betrieben werden.
- Bei Wechselspannung im Schaltstromkreis muss die Bauform des Relais stimmen (geschichtete Bleche, sonst Hitzeentwicklung).
- Relais enthalten eine Spule --> Beim **Ausschalten des Relais** verbleibt **Energie** in der Spule.
- Die Induktionsspannung wirkt dem Spannungsaufbau entgegen "der Strom will weiter fließen".
- Eine Freilaufdiode verhindert, dass sich die Energie der Spule in Funken oder ähnlichem entlädt und Bauteile und Platine zerstören kann.
- Daher: **RELAIS NUR MIT FREILAUFDIODE BETREIBEN**
- <img src="img/2.Passive_Bauteile/relai2.png" height="auto" width="auto">
- **Diode**
  - Freilaufventil, lässt Strom nur in eine Richtung durch

### Elektromechanische Ventile
- Das Prinzip des Relais kann auch auf
  - Pneumatische Ventile
  - Hydraulische ventile
  - Flüssigkeitsventile
  - Gasventile
  - etc
übertragen werden.
- <img src="img/2.Passive_Bauteile/ventile.png" height="auto" width="auto">
- Ventile können sehr robust ausgeführt werden.

### Schalter und Taster
- **Taster**: kurzzeitiges Umschalten auf einen anderen Zustand
  - **Art*: Öffner, Schließer, Umschalter
  - **Anzahl der Pole**: 1-Polig, 2-Polig, ..
  - **Rastend oder nicht rastent**: springt mit einer Feder wieder in die Ausgangsposition zurück z.b. Telefon. Rastende Taster sind eigentlich Schalter.
  - <img src="img/2.Passive_Bauteile/taster.png" height="auto" width="auto">
  - <img src="img/2.Passive_Bauteile/taster2.png" height="auto" width="auto">
  - <img src="img/2.Passive_Bauteile/taster3.png" height="auto" width="auto">
- **Schalter**: 2 oder mehrere Dauerzustände
  - Für jeden Typ von Schalter wurden Schaltsymbole definiert. Nicht immer Konform.
  - <img src="img/2.Passive_Bauteile/schalter.png" height="auto" width="auto">
  - Auch bei Schaltern unterscheided man verschiedene Typen
    - **Art der Schaltung**: Schließer/Öffner, Umschalter, Drehschalter, Stufenschalter
    - **Anzahl der Pole**: 1-Polig, 2-Polig,...
  - <img src="img/2.Passive_Bauteile/schalter2.png" height="auto" width="auto">
  - **Kippschalter**
    - ON/OFF
    - ON/OFF/ON
    - ON/ON/ON
    - etc
    - <img src="img/2.Passive_Bauteile/schalter3.png" height="auto" width="auto">
  - **Drehschalter**
    - Clevere verschaltungen sind möglich
    - Unterscheidung:
      - Anzahl Pole
      - Anzahl Schaltstellung pro Pol
      - Anzahl der Ebenen
      - <img src="img/2.Passive_Bauteile/schalter4.png" height="auto" width="auto">

</details>

<details>
<summary><strong>3. Aktive Bauteile / Halbleiter</strong></summary>

### Halbleiter
- In Metallen sind Elektronen frei beweglich. Dies kann man als **Elektronengas** im Leiter ansehen.
- Die Elektronenhüllen der Atome haben **verschiedene Energieniveaus**. Das oberste Band beschreibt die beweglichen Elektronen. In den Bändern darunter können Elektronen nicht so einfach gelöst werden.
- Das Energieniveau der **Elektronen, die beweglich** sein können, wird als **Valenzband** bezeichnet.
- <img src="img/3.Aktive_Bauteile/valenz.png" height="auto" width="auto">
- Das **Leitungsband** ist im Grundzustand nicht mit Elektronen besetzt. Man kann es sich als die Energie vorstellen, die nötig ist, um eine Elektronenwanderung zu ermöglichen.
- **Zwischen Valenz- und Leitungsband befindet sich eine Bandlücke oder Bandabstand**
- Die Breite der Bandlücke hat direkten Einfluss** auf die Leitfähigkeit.
- Je **größer der Bandabstand, desto geringer die Leitfähigkeit**
  - Bei einem Leiter ist der Bandabstand sehr klein, bei einem Isolator groß und bei einem Halbleiter dazwischen.
- Bei **Halbleitern** lösen sich schon bei **Raumtemperatur Elektronen** aus dem **Valenzband** ins **Leitungsband** (und sind somit frei). Nach **anlegen einer Spannung ensteht so ein Stromfluss**.
- <img src="img/3.Aktive_Bauteile/valenz2.png" height="auto" width="auto">
- **Dotierte Halbleiter**
  - In kristalliner reiner Form finden Halbleiter **wenig** Einsatz.
  - Erst durch gezielte **"Verschmutzung" von hochreinen Halbleiterkristallen** mit Elementen der 3. oder 5. Hauptgruppe, **entstehen dotierte Halbleiter**, die eine enorme technische Relevanz haben.
  - <img src="img/3.Aktive_Bauteile/valenz3.png" height="auto" width="auto">

### Diode
- **n-dotierte Halbleiter** haben somit "wandernde Elektronen" - also **freie Elektronen**.
- **p-dotierte Halbleiter** weisen "wandernde Löcher" auf, also Möglichkeiten für Elektronen zum Andocken - **quasi wandernde positive Ladungen**.
- Durch **Anschließen** einer Spannung entsteht ein **Stromfluss** (wozu aber wieder der Bandabstand überwunden werden muss).
- Bringt man einen **p-dotierten und einen n-dotierten Halbleiter zusammen**, so entsteht eine Kontaktfläche der beiden Materialien.
- Das resultierende Bauteil ist eine **Halbleiterdiode**.
- <img src="img/3.Aktive_Bauteile/diode.png" height="auto" width="auto">
- <img src="img/3.Aktive_Bauteile/diode2.png" height="auto" width="auto">
- **Übersteigt die Sperrspannung** einen bestimmten Wert, so **bricht die Diode durch**
- Dioden können also nur in einem bestimmten Spannungsbereich eine Ventilwirkung bereitstellen.
- **An Siliziumdioden fallen in Durchlassrichtung ca 0,7V ab.**
- <img src="img/3.Aktive_Bauteile/diode3.png" height="auto" width="auto">
- <img src="img/3.Aktive_Bauteile/diode4.png" height="auto" width="auto">
- **Einsatz von Dioden: Brückengleichrichter / ODER**
  - Brückengleichrichter "spiegeln" die negative Halbwelle einer Wechselspannung.
  - **Grundbaustein** für alle Arten von **Netzteilen**, die **Transformatoren** nutzen.
  - <img src="img/3.Aktive_Bauteile/diode5.png" height="auto" width="auto">
- **LED - Light Emitting Diode (Leucht-Effekt-Diode)**
  - <img src="img/3.Aktive_Bauteile/led.png" height="auto" width="auto">
  - Eine Leuchtdiode besteht aus einem **n-leitenden Grundhalbleiter**. Darauf ist eine **sehr dünne p-leitende Halbleiterschicht mit großer Löcherdichte** aufgebracht.
  - Wie bei der normalen Diode wird die Grenzschicht mit freien Ladungsträgern überschwemmt.
  - Die **Elektronen rekombinieren mit den Löchern** an der Grenzfläche.
  - Dabei geben die Elektronen ihre Energie als Phonon (Gitterschwingung) in **Form von (Licht-)Quanten frei.**
  - Die **p-Schicht ist sehr dünn** ausgeführt, sodass Licht hindurch entweichen kann.
  - Bereits bei kleinen Stromstärken ist eine Lichtabstrahlung wahrnehmbar.
  - Die **Lichtstärke wächst proportional mit der Stromstärke**.
  - Da von dem Halbleiterkristall nur eine geringe Lichtstrahlung ausgeht, ist das Metall unter dem Kristall **halbkugelförmig** ausgeführt, wodurch **das Licht gestreut wird**.
  - Durch das **linsenförmige Gehäuse wird das Licht gebündelt**.
  - <img src="img/3.Aktive_Bauteile/led2.png" height="auto" width="auto">
  - Die **Leuchtfarbe der LED** ergibt sich aus den Halbleitermaterialien, zw. aus dem **Bandabstand** der n/p-Teile.
  - Je nach Bandabstand, **"fallen die Elektronen im Grenzbereich unterschiedlich hoch" --> verschiedene Energieniveaus.**
  - <img src="img/3.Aktive_Bauteile/led3.png" height="auto" width="auto"> <img src="img/3.Aktive_Bauteile/led4.png" height="auto" width="auto">
  - Wird eine **größere Spannung** als die angegebene angelegt, so fließt ein zu **großer Strom durch die LED**, was zu deren **Zerstörung** führt.
  - Daher werden LEDs mit **Vorwiederstand** oder mit **Konstantstromquelle** betrieben.
  - <img src="img/3.Aktive_Bauteile/led5.png" height="auto" width="auto">
  - LEDs sind **besser und effizienter** als Glühbirnen, jedoch entsteht mehr Abwärme. Deswegen werden LEDs oftmals **gepulst** betrieben.
  - Rot, Grün und Gelbe LEDs können easy hergestellt werden.
  - Bei der herstellung von **Blauen LEDs** war der **große Bandabstand eine Herausforderung**, weil dieser für "blaue Quanten" notwendig ist.
  - Erst durch die **Verfügbarkeit von blauen LEDs( seit erst ca 10Jahren)**, war es möglich, **weiße LEDs herzustellen**.
  - **Weiße LEDs**
    - Längere Lebensdauer
    - Gleichmäßiges Licht
    - Einstellbare Lichttemperatur durch die Phosporschicht der Blauen LED.
  - **OLED - Organic LED**
    - **Biegsamer Träger und kostengünstig**.
    - Displays mit OLEDs
      - **Aktiv leuchtende Displays** umsetzbar (ohne Hintergrundbeleuchtung)
      - **Blickwinkelunabhängig**
      - **sehr viel weniger Energieverbauch** als TFT-Displays bei gesteigerter farbwiedergabe
      - <img src="img/3.Aktive_Bauteile/oled.png" height="auto" width="auto">


### Bipolartransistoren
- <img src="img/3.Aktive_Bauteile/transistorx.png" height="auto" width="auto">
- <img src="img/3.Aktive_Bauteile/transistorx2.png" height="auto" width="auto">
- Steuern **mit einem kleinen Strom einen großen Stromfluss**
- **Verstärken Ströme**
  - In den **Kollektor** fließt viel Strom, in die **Basis** wenig Strom --> Aus dem **Emitter** fließen beide Ströme. Also vereinigt der Transistor Kollektor- und Basisstrom und gibt den Strom am Emitter wieder aus.
  - Je nach Flussrichtung ist es genau andersrum --> Strom fließt in den **Emitter** und von dort aus der **Basis** und dem **Kollektor** wieder heraus. (tatsächliche fließrichtung --> von negativ nach positiv)
- Bestehen aus 3 Schichten aus dotiertem Material. Entweder **PNP** oder **NPN**
- <img src="img/3.Aktive_Bauteile/transistor.png" height="auto" width="auto">
- <img src="img/3.Aktive_Bauteile/transistor3.png" height="auto" width="auto">
- <img src="img/3.Aktive_Bauteile/transistor4.png" height="auto" width="auto">
- Ohne Strom und Spannung am Transistor ensteht aufgrund der NPN dotierung ein Hügel, wo die Elektronen nicht drüber kommen.
- Wenn man jetzt eine Spannung am Kollektor und Emitter anlegt, bleibt immernoch ein Hügel, wodurch dann die Elektronen **auch nicht** drüber kommen.
- Erst, wenn mann an der Basis auch noch Spannung anlegt, fangen die Elektronen an zu fließen.

### Transistor als Schalter
  - Werden häufig zum **kontaktlosen Schalten** benutzt.
  - Der Schalter ist dabei die **Kollektor-Emitter_Streck (CE)** des Bipolartransistor.
  - Die **Basis** dient als **Steuereingang**.
  - Liegt keine Basis-Spannung **Ube** an, so fließt auch kein Strom **Ic**
  - Da der Basisstrom **Ib** nur sehr wenig Strom benötigt, wird ein Basiswiederstand **Rv** eingebaut.
  - Der **Lastwiederstand Rc ist der Wiederstand der zu schaltenden Last**
  - <img src="img/3.Aktive_Bauteile/transistor5.png" height="auto" width="auto">
  - <img src="img/3.Aktive_Bauteile/transistor6.png" height="auto" width="auto">
  - <img src="img/3.Aktive_Bauteile/transistor7.png" height="auto" width="auto">
  - Die **Grüne Gerade - Arbeitsgerade, auf der sich der Transistor befindet -** sollte immer außerhalb der **tot-zone** liegen. Das kann man z.b. durch ändern des Basisstroms bewirken.

### Transistor als Verstärker
  - Bipolartransistoren werden zur Spannungs- und Stromverstärkung genutzt.
  - Meistens mit Vorwiederständen
  - verhalten des Transistors kann aus dem Ausgangskennlinienfeld abgeleitet werden.
  - <img src="img/3.Aktive_Bauteile/transistory.png" height="auto" width="auto">
  - **Fixed Base Bias**
  - <img src="img/3.Aktive_Bauteile/transistory2.png" height="auto" width="auto">
  - Weil jeder Transistor eine andere **Toleranz** hat, ist die Spannung direkt von **ß** abhängig.
  - Um den Einfluss von **ß** zu verringern, kann zusätzlich ein Wiederstand eingebaut werden sodass in der Gleichung **ß im Zähler und im Nenner steht**. --> Dadurch ist der Einfluss von **ß** reduziert.
</details>

<details>
<summary><strong>4. MOSFET</strong></summary>

- ### MOSFET - Metal Oxide Semiconductor Field Effect Transistor
  - Feldeffekttransistor mit isoliertem Gate
  - **Spannungsgesteuerter Transistor** --> Verstärken Strom in Abhängigkeit einer Steuerspannung.
  - **Vergleich Bipolartransistor: Collector-Steuerspannung vs Basisverstärker**
  - **Hohe Schaltfrequenzen**, weniger Verluste als Bipolartransistor.
  - Zur **Ansteuerung ist kein andauernder Strom** notwendig im Gegensatz zu Bipolartransistoren. --> stromsparende ICs (Integrated Circuits) umsetzbar.
  - **Schalten / Verstärken mit hohen Stromstärken** möglich. --> Ersatz von Relais.
  - **Idealer MOSFET: Es fließt kein Strom in das Gate**
  - <img src="img/4.MOSFETS/mosfet.png" height="auto" width="auto">
  - Bei **Source gehen die Elektronen rein**
  - Bei **Drain gehen die Elektronen raus**
  - Beim **Gate fließt einmal was rein und dann sogut wie nichts mehr**
  - **Typen**
    - **n-Kanal (NMOS)** - für **Positive** Spannungen (vgl. NPN-Bipolartransistor)
    - **n-Kanal (PMOS)** - für **negative** Spannungen (vgl. PNP-Bipolartransistor)
    - In Kombination **CMOS (Complementary MOS)** -_> Wird heute in Microcontrollern verbaut.
    - NMOS und PMOS können unterschiedliche Eigenschaften haben:
      - #### Verarmungstyp selbstleitend (normal leitend)
        - Wenn keine Steuerspannung anliegt, leitet der Verarmungstyp
        - Muss aktiv durch eine Sperrspannung abgeschaltet werden und bleibt dann so
      - #### Anreicherungstyp selbstsperrend (normal sperrend)
        - Wenn keine Steuerspannung anliegt, sperrt der Anreicherungstyp
        - Muss aktiv eingeschaltet werden und bleibt dann so
      - #### Anreicherungstyp NMOS
        - <img src="img/4.MOSFETS/mos1.png" height="auto" width="auto">
      - #### Anreicherungstyp PMOS
        - <img src="img/4.MOSFETS/mos5.png" height="auto" width="auto">
    - #### CMOS - Beispiel Inverter
        - <img src="img/4.MOSFETS/mos2.png" height="auto" width="auto">
        - <img src="img/4.MOSFETS/mos3.png" height="auto" width="auto">
    - #### Low-Side/High-Side
        - **Low-Side Schalter:** Der FET schaltet eine Last gegen GND - auch als LS-Schalter bezeichnet.
        - <img src="img/4.MOSFETS/mos4.png" height="auto" width="auto">
        - **High-Side Schalter:** Der Fet schaltet eine Last an die Versorgungsspannung (+) - auch als HS-Schalter bezeichnet.

    - #### smartFETs
        - Ein smartFET misst den Source-Drainstrom und gibt einen proportionalen Strom oder Spannung aus.
            - High-Side/Low-Side-Überwachung
        - **Nutzen**:
          - Erkennen, wieviel Strom fließt durch Messung einer Spannung mit einem A/D-Wandler,
          - zum Erkennen von defekten Glühbirnen im KFZ oder Verbrauchern
          - Stormüberwachung (Elektrische Fensterheber)
          - Überstromschutz
</details>

<details>
<summary><strong>5. Wechselstrom</strong></summary>

<img src="img/5.Wechselstrom/w1.png" height="auto" width="auto">
<img src="img/5.Wechselstrom/w2.png" height="auto" width="auto">

### Wiederstand an Wechselspannung
  - Verhalten eines Wiederstands beim Anlegen von Wechselspannung:
    - <img src="img/5.Wechselstrom/w3.png" height="auto" width="auto">
  - Keine Phasenverschiebung zwischen Strom und Spannung
    - <img src="img/5.Wechselstrom/w4.png" height="auto" width="auto">

### Kondensator an Wechselspannung
  - Die passiven Bauteile Wiederstand, Kondensator und Spule haben verschiedene Verhaltensweisen, wenn sie mit Wechselspannung betrieben werden, bzw. in Schaltungen betrieben werden, die mit Wechselspannung arbeiten
  - Signalverarbeitung, Spannungsversorgung, uvm.
    - <img src="img/5.Wechselstrom/w5.png" height="auto" width="auto">
  - Verhalten eines Kondensators beim Anlegen von Wechselspannung:
    - <img src="img/5.Wechselstrom/w6.png" height="auto" width="auto">

### Komplexe Wechselstromrechnung
  - Durch Einführung von komplexen Größen kann die Phasenverschiebung und die Änderung der Amplitude in Abhängigkeit von der Frequenz berechnet werden
  - <img src="img/5.Wechselstrom/w7.png" height="auto" width="auto">
  - Der Betrag und die Phasenverschiebung in Schaltungen mit Kondensatoren und Spulen und Wiederständen errechnet sich mit Hilfe der komplexen Rechnung - man nimmt dazu die komplexen Wiederstände an und wendet die **Maschenregel** an.
  - Die Phasenverschiebung ist der Winkel zwischen der reelen und der imaginären Achse.
  - Der Betrag errechnet sich aus den imaginären und reelen Anteilen.
  - <img src="img/5.Wechselstrom/w13.png" height="auto" width="auto">
  - Interessant ist für viele Schaltungen, wie diese reagiert, wenn eine Frequent variiert wird.
  - Das Verhalten der Schaltung wird im Amplitudengang ersichtlich.
  - Dies kann mit PSPICE simuliert werden.
  - <img src="img/5.Wechselstrom/w17.png" height="auto" width="auto">

### Einfache Hoch/Tief/Bandpässe
  - In vielen Anwendungen sollen Frequenzen ausgefiltert oder verstärkt werden.
  - Beispiele:
    - Equalizer an der Hifi-Anlage
    - Nachrichtentechnik
    - Alle Arten von Signalverarbeitung
  - Es wird zwischen passiven und aktiven Hoch/Tief/Bandpässen unterschieden:
    - **Passive Ausführungen** enthalten ausschließlich passive Bauteile, in den allermeisten Ausführungen **Kondensatoren und/oder Spulen und Wiederstände**
    - **Aktive Ausführungen** enthalten zusätzlich **Verstärkerbausteine** (Transistoren, OPV).
  - Die Auswirkungen eine Hoch/Tief/Bandpasses in Abhängigkeit zur Frequenz wird durch den Bode-Plot visualisiert.

### Passiver Hochpass / Passiver Hochpassfilter
  - **Ein passiver Hochpass dämpft niedrige Frequenzen und lässt hohe Frequenzen durch**
  - <img src="img/5.Wechselstrom/w8.png" height="auto" width="auto">
  - **Passiver Tiefpass / Passiver Tiefpassfilter**
    - **Ein passiver Tiefpass lässt die niedrigen Frequenzen durch und dämpft hohe Frequenzen**
    - **Ausführung als R-C-Glied: Wie oben beschrieben, hat ein Kondensator folgenden komplexen Wiederstand:**
        - <img src="img/5.Wechselstrom/w8.png" height="auto" width="auto">

### Hochpass/Tiefpass mit Spulen
  - <img src="img/5.Wechselstrom/w10.png" height="auto" width="auto">
  - <img src="img/5.Wechselstrom/w11.png" height="auto" width="auto">

### Bandpass und Bandsperre
  - Ein Bandpass dämpft tiefe und hohe Frequenzen ab und lässt die Frequenzen "in der Mitte" durch.
  - Die Bandsperre filtert einen gewissen Frequenzbereich aus.
  - <img src="img/5.Wechselstrom/w12.png" height="auto" width="auto">
  - <img src="img/5.Wechselstrom/w14.png" height="auto" width="auto">
  - <img src="img/5.Wechselstrom/w16.png" height="auto" width="auto">

### Dezibel - dB
  - Bode-Diagramme zeichnen zum einen einen solchen Amplitudengang als Verhätlnis einer Ausgangs-zu- einer Eingangsspannung.
  - <img src="img/5.Wechselstrom/w18.png" height="auto" width="auto">

### Berechnung von Ketten von Verstärkern
  - <img src="img/5.Wechselstrom/w19.png" height="auto" width="auto">
- <img src="img/5.Wechselstrom/w20.png" height="auto" width="auto">
  - Verstärkung in **dB**
    - <img src="img/5.Wechselstrom/w21.png" height="auto" width="auto">

### Phasengang
  - Der Phasengang reicht von 180° und dreht quasi bei der Resonanzfrequenz zu 0°
  - <img src="img/5.Wechselstrom/w22.png" height="auto" width="auto">

### Phasenverschiebung / Sinussignale
  - Die **Phasenverschiebung** zwischen **Strom und Spannung** führt dazu, dass ein Teil der **Leistung P=UI** in **Wirkleistung (Realteil) und Blindleistung (Imaginärteil aufgeteilt wird**.
  - Die Blindleistung ist problematisch, da hierfür mechanische Arbeit im Kraftwerk aufgebracht werden muss, aber diese nicht abgerechnet wird (der Stromzähler "zählt" die Wirkleistung).
    - Falls ein Gerät aufgrund einer Spule (z.b. Motor) eine Blindleistung erzeugt, kann durch Einbau eines Kondensators die Phase wieder "nach vorne" geschoben werden.
    - **Deshalb sind in Motorgetriebenen Geräten oft Drosseln (Spulen) oder Kondensatoren verbaut, um die Phasenverschiebung Strom/Spannung wieder zu korrigieren.**
    - Im Kleinsignalbereich (z.b. Audioverstärker) ist dies unerheblich, da dies hier nicht auf die Spannungsversorgung wirkt.
    - **Grundprinzip:**
        - **Alle Signalformen können durch eine Addition von Sinussignalen verschiedener Frequenzen zusammengesetzt werden.**
    - Somit kann ein Gesamtverhalten einer Schaltung ermitttelt werden.
    - Zum Beispiel sind Audiosignale eine Summe vieler Sinuslinien. Der Frequenzverlauf gibt an, welche Frequenzen verstärkt und welche abgeschwächt werden.
    - Auch in der Signalverarbeitung und Nachrichtentechnik können so Frequenzen gezielt verstärkt oder gedämpft werden.
</details>

<details>
<summary><strong>6. Operationsverstärker (OPV)</strong></summary>

### Einsatz
- Spannungspegel **können verstärkt oder reduziert** werden.
- **Signalverarbeitung**: Integrierer, Differenzierer, Equalizer, Analogrechner
- Addition, Subtraktion, Multiplikation von Spannungen
- **Impedanzwandler**
- **Komparatoren**
- **Kostengünstige Bausteine mit sehr guter Signalqualität**
    - sehr starke Verbreitung in der Analogtechnik und Analogteilen von Digitalschaltungen
- Oftmals werden Operationsverstärker als Spannungsverstärker eingesetzt:
- <img src="img/6.Operationsverstärker/o1.png" height="auto" width="auto">

### Eigenschaften
- Bei der Modellierung von **OPV** wird sehr häufig von dem **idealen OPV** ausgegangen. Moderne Bautypen kommen diesem sehr nahe.
- Der **ideale OPV** hat folgende Eigenschaften:
  - **Keine Eingangsströme**
  - **Ausgangsstrom beliebig groß**
  - **Verstärkung der Differenzsspannung Ud unendlich --> Daher die Annahme: Ud=0V**
  - <img src="img/6.Operationsverstärker/o2.png" height="auto" width="auto">
  - Beispiel: <img src="img/6.Operationsverstärker/o3.png" height="auto" width="auto">

### Nichtinvertierender Verstärker
- Geeignet für Spannungsverstärkung für Verstärkungen **k > 1**
- Durch den Eingang am OPV fließt kein Strom in den Operationsverstärker.
- Oftmals geben Sensoren, Schnittstellen, aktive Bauelemente Spannungen aus, deren Pegel an einen Eingang angepasst werden muss. Beispiel: Ein Sensor liefert 0-1V als Spannungssignal. Zum Anschluss an einen A/D-Wandler mit einer Eingangsspannung von 0-5V wird eine Verstärkung von **k = 5** gewählt, um den gesamten Messbereich auszunutzen.
- <img src="img/6.Operationsverstärker/o4.png" height="auto" width="auto">
- <img src="img/6.Operationsverstärker/o5.png" height="auto" width="auto">
- <img src="img/6.Operationsverstärker/o6.png" height="auto" width="auto">

### Invertierender Verstärker
- Geeignet für Spannungsverstärkungen für **k >= 1** und auch **k < 1** (Dämpfung)
- Durch den Eingang am OPV **fließt kein Strom** in den OPV
- <img src="img/6.Operationsverstärker/o7.png" height="auto" width="auto">
- <img src="img/6.Operationsverstärker/o8.png" height="auto" width="auto">

### Impedanzwandler
- **Entkoppeln** eine Schaltung
- **Eingangsstrom ist Null**
- **Hochohmiger Eingang, Niedrigohmiger Ausgang** ("Wandlung der Impdeanz")
- **Einsatz:**
    - Entkopplung von Leitungsverlusten
    - Entkoppeln von Ein- und Ausgängen
    - Entkoppeln von Signalen in Analogschaltungen
- <img src="img/6.Operationsverstärker/o8.png" height="auto" width="auto">

### Komparatorschaltung
- Komparatoren werden genutzt, um beim Überschreiten einer Eingangsspannung ein High-Signal zu erhalten.
    - Nutzung als Schwellschalter (Bsp.: Temperatur übersteit bestimmten Wert --> High-Pegel, verhalten wie Thermostat)
    - 1-Bit A/D-Wandler (Schwellwertschalter zum Einlesen an einem digitalen Port)
- <img src="img/6.Operationsverstärker/o9.png" height="auto" width="auto">

### Referenzspannungsquelle
- **Entkoppeln einer Reihenschaltung aus Widerstand und Zener-Diode mit Impedanzwandler**
- Spannung, die an einer Zenerdiode abfällt, hängt von Strom durch Zenerdiode ab.
    - Arbeitspunkt einstellen über **R**
- Strom fließt ausschließlich durch Widerstand R, da Eingangsstrom in OPV gleich null
- <img src="img/6.Operationsverstärker/o10.png" height="auto" width="auto">

### Integrator
- **Integration einer Spannung Ue**
- Ausgabe an **Ua**
- Wird oft zur Signalverarbeitung genutzt.
- <img src="img/6.Operationsverstärker/o11.png" height="auto" width="auto">
- <img src="img/6.Operationsverstärker/o12.png" height="auto" width="auto">

### Differenzierer
- Leitet **Ue** ab und gibt das Ergebnis an **Ua** aus.
- Damit kann mit Spannungsänderungen gearbeitet werden:
    - Ändert sich **Ue schnell**, so wird **Ua groß**.
    - Ändert sich **Ue langsam**, so wird **Ua klein**.
    - **Ue = konstant, so ist Ua = 0**
    - Ändert sich **Ue linear**, so ist **Ua = konstant**
- <img src="img/6.Operationsverstärker/o13.png" height="auto" width="auto">

### Aktive Filter - Beispiel: Tiefpassfilter
- Mit Operationsverstärkern (OPV) können aktive Filter für Wechselspannungen (Audio, messtechnik, etc) aufgebaut werden.
- Hierzu werden Kondensatoren (und in seltenen Fällen Spulen) in den Rückkoppelzweig geschalten.
- Das Feld der aktiven Schaltungen ist extrem groß, daher ein abschließendes Beispiel:
- <img src="img/6.Operationsverstärker/o14.png" height="auto" width="auto">

</details>

<details>
<summary><strong>7. Digitaltechnik</strong></summary>

### Digitalbausteine
- Digitalbausteine werden meist in folgenden Einsatzgebieten eingesetzt:
    - In der **Peripherie** von Microcontrollern
    - **Auslagerung von Aufgaben** aus einem Microcontroller
    - **Standalone Geräte** ohne Microcontroller (für einfach Aufgaben)
    - **Ansteuerung von Peripherie** wie LCD/LED-Anzeigen (Treiber etc)
- **Zu Beginn** der Einführung der Digital-ICs in der Elektronik wurden häufig **TTL-ICs** (Transistor-Transistor-Logik) der 74xx-Baureihe eingesetzt. Diese haben aber einige Nachteile:
    - Basieren auf Bipolar-Transistoren (brauchen Basisstrom)
    - High entspricht 0V, Lo entspricht 5V.
    - Betriebsspannung ist fet (meist 5V)
    - Stromverbrauch ist relativ hoch (nix gut für Batteriebetriebene Geräte)
    - Inzwischen sind die ICs aufgrund der geringen Stückzahl relativ teuer (mehrere €)
- Viele der genannten Nachteile weisen **CMOS-ICs der 40xx** Baureihe nicht auf.
    - **Variable Spannungsversorgung** von ca 3V bis 20V
    - Basieren auf **MOSFETs** und weisen daher sehr geringen Stromverbrauch auf. --> **Spannungsgesteuert (MOSFET) vs stromgesteuert (Bipolar-Transistoren)**
    - **Kostengünstig**
    - **NACHTEIL: Empfindlicher gegen elektronische Aufladung**

### Handhabung von CMOS-ICs
  - **CMOS Chips können beim Berühren der Pins zersört werden**
    - Entladung der Hände kann Abhilfe schaffen
  - **Anschluss der Spannungsversorgung Merksatz:**
    - <img src="img/7.Digitaltechnik/d1.png" height="auto" width="auto">
  - **Anschluss von tastern/Schaltern**
    - <img src="img/7.Digitaltechnik/d2.png" height="auto" width="auto">
  - **Ausgänge**
    - Die Ausgänge können **nicht mit größeren Verbrauchern belastet werden**, da die **ICs** nur für kleine Ströme ausgelegt sind. Zum Betrieb von vielen LEDs, Motoren, Aktuatoren etc müssen Transistorstufen zwischengeschaltet werden.
    - Wird ein **Ausgang eines Gatters an einen Eingang eines anderen Gatters angeschlossen**, so wird **kein nPull-Up/Pull-Down-Widerstand** benötigt, weil die Pegel eindeutig High oder Lo sind.

### Logikgatter
- <img src="img/7.Digitaltechnik/d3.png" height="auto" width="auto">
- <img src="img/7.Digitaltechnik/d4.png" height="auto" width="auto">

### Flip-Flops
- **Flip-Flops sind 1-Bit Speicher** in verschiedenen Ausführungen
- <img src="img/7.Digitaltechnik/d5.png" height="auto" width="auto">

### Zeitschaltungen
- In Verbindung mit **Quarzen** und **Quarzoszillatoren** lassen sich präzise zeitliche Abläufe mit Digitalbausteinen automatisieren bzw umsetzen.
- <img src="img/7.Digitaltechnik/d6.png" height="auto" width="auto">

### Zeitschaltungen mit Oszillatoren
- Wahl eines Quarzoszillators und eines Zählerbausteins zur Ableitung eines Zeittaktes.
- **Beispiel: 32768Hz oder 32768kHz**
    - Wird ein Zähler mit einer Frequenz von 32768Hz betrieben, so vergeht nach 32768 Zählerschritten genau eine Sekunde
    - Bei einer Frequenz von 32768kHz vergehen in 32768 Zählerschritten genau 1 Millisekunde
- **Beispiel**: 4184304Hz = 2^22
    - Ein Zählerbaustein, der bis 2^22 zählen kann in Verbindung mit einem Quarzoszillator, kann eine einfach Sekundenbasis bereitstellen.
    - **CMOS 4521 ist ein 24Stufiger Binärzähler**
    - <img src="img/7.Digitaltechnik/d7.png" height="auto" width="auto">
- **Taktgeber mit 4521**
    - <img src="img/7.Digitaltechnik/d8.png" height="auto" width="auto">

### Netzteil mit Linear Spannungsregler
- Festspannungsregler dienen dazu, eine definierte Spannung am Ausgang zu erhalten unabhängig von Stromschwankungen am Ausgang und Spannungschwankungen am Eingang. (Filter/Stützkondensatoren notwendig)
- Die Eingangsspannung sollte etwa mindestens 2V größer als die gewünschte Ausgangsspannung sein.
- Der Brückengleichrichter spiegelt bei **AC** negative Halbwellen zu positiven und dient bei **DC** als Verpolschutz.
- <img src="img/7.Digitaltechnik/d9.png" height="auto" width="auto">
- Linearregler sind einfach und kostengünstig. Effizientere Bausteine nutzen **DC/DC** Converter, wandeln die Spannungen mit weniger Verlust. Viele **DC/DC** Wandler erlauben auch eine von kleinen zu großen Spannungen.
</details>

<img src="img/Schmitttrigger.jpg" height="auto" width="auto">
